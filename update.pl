#!/usr/bin/env perl

# Copyright © 2021 by Jeff Vos <jeff@jeffvos.dev>
# ⚔️

use strict;
use warnings;
use utf8;
use v5.10;
use Getopt::Std;

$| = 1;

my $apt_cmd = 'apt-get';
my @check_apt = ('which', $apt_cmd,);
my @apt_update = ($apt_cmd, 'update',);
my @apt_upgrade = ($apt_cmd, 'upgrade',);
my @apt_autoremove = ($apt_cmd, 'autoremove',);
my %options = ();

sub check_user {
	my $user_name = getlogin();
	if($user_name ne 'root') {
		say "Not run as root; attempting to use 'sudo'.";
		unshift @check_apt, 'sudo';
		unshift @apt_update, 'sudo';
		unshift @apt_upgrade, 'sudo';
		unshift @apt_autoremove, 'sudo';
	}
}

sub set_options {
	getopts("y", \%options);
	if(defined $options{y}) {
		say 'Running in noninteractive mode.';
		push @apt_upgrade, '-y';
		push @apt_autoremove, '-y';
	}
}

sub main() {
	check_user();
	print 'Executable: ';
	system(@check_apt) == 0
		or die "not found: $check_apt[-1]\n";
	set_options();
	say 'Beginning update procedure.';
	system @apt_update;
	system @apt_upgrade;
	system @apt_autoremove;
}

main();
